CREATE TABLE customer
(
  customer_id int PRIMARY KEY NOT NULL,
  customer_email_id varchar(255) NOT NULL,
  first_name varchar(50),
  last_name varchar(50),
  password varchar(50),
  created_date datetime,
  updated_date datetime
);

CREATE TABLE product
(
  product_id int PRIMARY KEY NOT NULL,
  product_name varchar(255) NOT NULL,
  description text,
  created_date datetime,
  updated_date datetime
);
