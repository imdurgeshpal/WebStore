package com.webstore.controllers;

import com.webstore.model.LoginForm;
import com.webstore.model.Product;
import com.webstore.services.AdminService;
import com.webstore.services.ProductServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.naming.Binding;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/admin/")
public class AdminController {

    @Resource
    private AdminService adminService;

    @Resource
    private ProductServices productServices;

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String getAdminLogin(HttpServletRequest request, HttpServletResponse response, @ModelAttribute LoginForm loginForm) {
        return "@admin-login";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public  String adminLogin(HttpServletResponse response, HttpServletRequest request, @ModelAttribute LoginForm loginForm, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "@admin-login";
        }
        else if (adminService.loginAdmin(loginForm)){
            return "@admin-dashboard";
        }
        return "@admin-login";
    }

    @RequestMapping (value = "viewProducts", method = RequestMethod.GET)
    public String viewProduct(ModelMap modelMap){
        modelMap.addAttribute("products", productServices.showProduct());
        return "@viewProducts";

    }
}
