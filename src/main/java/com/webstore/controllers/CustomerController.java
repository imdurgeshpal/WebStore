package com.webstore.controllers;

import com.webstore.model.Customer;
import com.webstore.model.LoginForm;
import com.webstore.model.Product;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.webstore.services.CartServices;
import com.webstore.services.ProductServices;
import com.webstore.services.CustomerServices;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping (value = "/customer/")
public class CustomerController {

    @Resource
    private CustomerServices customerServices;

    @Resource
    private ProductServices productServices;

    @Resource
    private CartServices cartServices;


    //	This is for index page.
    @RequestMapping("signup")
    public ModelAndView getSignupForm() {
        return new ModelAndView("@customer-signup", "signUpForm", new Customer());
    }

    //    This is for addUser Page
    @RequestMapping(value = "signup", method = RequestMethod.POST)
    public ModelAndView SignUp(@ModelAttribute("signUpForm") @Valid Customer customer, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("@customer-signup");
        } else {
            customerServices.saveCustomer(customer);
            return new ModelAndView("redirect:login");
        }
    }

    //    This is for update
    @RequestMapping(value = "/update/{id}")
    public ModelAndView edit(@PathVariable long id) {
        Customer customer = customerServices.findUserbyID(id);
        return new ModelAndView("updateUser", "command", customer);
    }

    @RequestMapping(value = "/saveUpdate", method = RequestMethod.POST)
    public ModelAndView update(@ModelAttribute("update") Customer customer) {
        customerServices.updateUser(customer);
        return new ModelAndView("redirect:/viewUser");
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable long id) {
        customerServices.deleteUser(id);
        return new ModelAndView("redirect:/viewUser");
    }


    @RequestMapping("/viewUser")
    public ModelAndView viewemp() {
        List<Customer> listCustomer = customerServices.showUser();
        return new ModelAndView("viewUser", "listCustomer", listCustomer);
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView getCustomerLogin() {
        return new ModelAndView("@customer-login", "loginForm", new LoginForm());
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ModelAndView login(@ModelAttribute("loginForm") LoginForm loginForm, HttpSession session) {

        if (customerServices.validateLogin(loginForm)) {
            session.setAttribute("loggeduser", loginForm.getEmail());
            ModelMap modelMap = new ModelMap();
            return new ModelAndView("redirect:dashboard");
        } else
            return new ModelAndView("redirect:/notlogin");
    }

    @RequestMapping("/notlogin")
    public ModelAndView notlogin() {
        return new ModelAndView("notlogin", "user", new Customer());
    }

    //  This is for index page.
    @RequestMapping("/dashboard")
    public ModelAndView home(HttpSession session, Customer customer) {
        session.setAttribute("loggeduser", customer.getCustomerEmail());
        List<Product> listProduct = productServices.showProduct();
        ModelMap modelMap = new ModelMap();
        modelMap.put("customer",customer.getCustomerEmail());
        return new ModelAndView("@customer-dashboard", "listProduct", listProduct);
    }
}