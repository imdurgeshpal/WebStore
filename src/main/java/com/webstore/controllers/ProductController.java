package com.webstore.controllers;

import com.webstore.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.webstore.services.CartServices;
import com.webstore.services.ProductServices;
import com.webstore.services.CustomerServices;

import java.util.List;

@Controller
public class ProductController {

    @Autowired
    CustomerServices customerServices;

    @Autowired
    ProductServices productServices;

    @Autowired
    CartServices cartServices;


    @RequestMapping("/addProduct")
    public ModelAndView product() {
        return new ModelAndView("addProduct", "product", new Product());
    }

    @RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
    public ModelAndView saveProduct(@ModelAttribute("product") Product product) {
        productServices.saveProduct(product);
        return new ModelAndView("redirect:/viewProduct");
    }

    @RequestMapping("/viewProduct")
    public ModelAndView viewproduct() {
        List<Product> listProduct = productServices.showProduct();
        return new ModelAndView("viewProduct", "listProduct", listProduct);
    }

    @RequestMapping(value = "/updateProduct/{id}")
    public ModelAndView editProduct(@PathVariable int id) {
        Product product = productServices.productbyID(id);
        return new ModelAndView("updateProduct", "command", product);
    }

    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
    public ModelAndView updateProduct(@ModelAttribute("update") Product product) {
        productServices.updateProduct(product);
        return new ModelAndView("redirect:/viewProduct");
    }

    @RequestMapping(value = "/deleteProduct/{id}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable int id) {
        productServices.deleteProduct(id);
        return new ModelAndView("redirect:/viewProduct");
    }
}
