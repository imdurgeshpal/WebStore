package com.webstore.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.webstore.model.Customer;
import com.webstore.model.LoginForm;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CustomerDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    @PersistenceContext
    private EntityManager em;

    public void saveCustomer(Customer customer) {
        em.persist(customer);
    }

    public List<Customer> showUser() {
        List<Customer> customers = em.createQuery("SELECT a FROM Customer a", Customer.class).getResultList();
        return customers;
    }

    public Customer findUserbyID(long id) {
        String sql = "select * from user where id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<Customer>(Customer.class));
    }

    public void updateUser(Customer customer) {
        String sql = "update user set userName =? where id=?";
        jdbcTemplate.update(sql, new Object[]{ customer.getFirstName(), customer.getCustomerId()});
    }

    public void deleteUser(long id) {

        Customer del = em.getReference(Customer.class, id);
        em.remove(del);

    }

    public boolean validateLogin(LoginForm loginForm) {
        Customer customer = new Customer();
        boolean customerFound = false;
        String SQL_QUERY = " from Customer as o where o.customerEmail='" + loginForm.getEmail() + "'";
        List<Customer> list = em.createQuery(SQL_QUERY, Customer.class).getResultList();
        if ((list != null) && (list.size() > 0)) {
            customerFound = true;
        }
        return customerFound;
    }
}
