package com.webstore.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.webstore.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.webstore.model.Cart;
import com.webstore.model.CartItem;
import com.webstore.model.Product;

@Repository
@Transactional
public class ProductDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PersistenceContext
    private EntityManager em;

    public void saveProduct(Product product) {
        em.persist(product);
    }

    public List<Product> showProduct() {

        List<Product> list = em.createQuery("SELECT a FROM Product a", Product.class).getResultList();
        return list;
    }

    public void updateProduct(Product product) {

        String sql = "update product set productName =?,productDescription=?, productPrice=? where id=?";
        jdbcTemplate.update(sql, new Object[]{product.getProductName(), product.getProductDescription(), product.getProductPrice(), product.getId()});

    }

    public void deleteProduct(int id) {

        Product del = em.getReference(Product.class, id);
        em.remove(del);

    }

    public Product findProductbyID(int id) {
        return em.find(Product.class, id);
    }

    public Customer findIdForUser(String sessionValue) {
        String sql = "from User where userName='" + sessionValue + "'";
        List<Customer> customerList = em.createQuery(sql).getResultList();
        return customerList.get(0);
    }

    public List<Cart> findCartForUser(long userId) {
        String sql = "from Cart where id='" + userId + "'";
        List<Cart> userList = em.createQuery(sql).getResultList();
        return userList;
    }

    public void saveCartItem(CartItem cartItem) {
        em.persist(cartItem);

    }

    public void addToCart(Cart cart) {
        em.persist(cart);

    }
}
