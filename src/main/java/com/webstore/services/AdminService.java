package com.webstore.services;

import com.webstore.dao.AdminDao;
import com.webstore.model.LoginForm;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AdminService {

    @Resource
    private AdminDao adminDao;

    public boolean loginAdmin(LoginForm loginForm) {
        if (loginForm.getEmail().equals("admin")) {
            return true;
        } else {
            return false;
        }
    }
}
