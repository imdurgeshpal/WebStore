package com.webstore.services;

import com.webstore.dao.CartDao;
import com.webstore.model.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartServices {

    @Autowired
    CartDao cartDao;

    public Cart findCartbyID(int id) {
        return cartDao.findCartbyID(id);
    }

    public void addToCart(Cart cart) {
        cartDao.addToCart(cart);

    }

    public List<Cart> showCart() {
        return cartDao.showCart();
    }


}
