package com.webstore.services;

import com.webstore.dao.CustomerDao;
import com.webstore.model.Customer;
import com.webstore.model.LoginForm;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CustomerServices {

    @Resource
    private CustomerDao customerDao;

    public void saveCustomer(Customer customer) {
        customerDao.saveCustomer(customer);
    }

    public List<Customer> showUser() {
        return customerDao.showUser();
    }

    public void updateUser(Customer customer) {
        customerDao.updateUser(customer);
    }

    public void deleteUser(long id) {
        customerDao.deleteUser(id);

    }

    public Customer findUserbyID(long id) {
        return customerDao.findUserbyID(id);

    }

    public boolean validateLogin(LoginForm loginForm) {
        return customerDao.validateLogin(loginForm);
    }
}
