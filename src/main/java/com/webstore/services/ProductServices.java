package com.webstore.services;

import com.webstore.dao.ProductDao;
import com.webstore.model.Cart;
import com.webstore.model.CartItem;
import com.webstore.model.Product;
import com.webstore.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServices {

    @Autowired
    ProductDao productDao;

    public void saveProduct(Product product) {
        productDao.saveProduct(product);

    }

    public List<Product> showProduct() {

        return productDao.showProduct();
    }

    public void updateProduct(Product product) {
        productDao.updateProduct(product);

    }

    public void deleteProduct(int id) {
        productDao.deleteProduct(id);

    }


    public void findProductbyID(int id, String sessionValue) {

        Product product = productDao.findProductbyID(id);
        Customer customer = productDao.findIdForUser(sessionValue);
        long userId = customer.getCustomerId();
        List<Cart> cartList = productDao.findCartForUser(userId);

        if (cartList.size() < 1) {
            Cart cart = new Cart();
            CartItem cartItem = new CartItem();
            cartItem.setProduct(product);
            List<CartItem> cartItemsList = new ArrayList<CartItem>();
            cartItemsList.add(cartItem);
            cart.setCustomer(customer);
            cart.setProductsList(cartItemsList);
            productDao.saveCartItem(cartItem);
            productDao.addToCart(cart);
        } else {
            Cart userCart = cartList.get(0);
            CartItem cartItem = new CartItem();
            cartItem.setProduct(product);
            List<CartItem> cartItemsList = new ArrayList<CartItem>();
            cartItemsList.add(cartItem);
            userCart.setProductsList(cartItemsList);
            productDao.saveCartItem(cartItem);
            productDao.addToCart(userCart);
        }

    }

    public Product productbyID(int id) {
        return productDao.findProductbyID(id);

    }
}
