<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  Customer: Durgesh Pal
  Date: 26-10-2017
  Time: 01:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Login</title>
</head>
<body>
<div class="container">
    <form:form class="form-horizontal" action="login" method="post" modelAttribute="loginForm">
        <div class="control-group">
            <label class="control-label">Email id</label>
            <div class="controls">
                <form:input class="span3" path="email" maxlength="255"/>
                <form:errors path="email" cssClass="error"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
                <form:password class="span3" path="password" maxlength="50"/>
                <form:errors path="password" cssClass="error"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button class="btn btn-primary" type="submit">login</button>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>
