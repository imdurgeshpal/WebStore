<%--
  Created by IntelliJ IDEA.
  User: Durgesh Pal
  Date: 28-10-2017
  Time: 02:27
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Products</title>
</head>
<body>
<div class="container">
    <table>
        <thead>
        <td>
        <th>Name</th>
        <th>Desc</th>
        <th>Price</th>
        </td>
        </thead>
        <c:forEach var="product" items="${products}">
            <tr>
                <td>${product.productName}</td>
                <td>${product.productDescription}</td>
                <td>${product.productPrice}</td>
            </tr>
        </c:forEach>

    </table>
</div>
</body>
</html>
