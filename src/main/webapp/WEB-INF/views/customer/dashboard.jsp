<%--
  Created by IntelliJ IDEA.
  User: durgesh
  Date: 31/10/17
  Time: 12:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Dashboard</title>
    </head>
    <body>
        <div class="container">

            <a class="btn btn-link text-center" href="mycart">My Cart</a>
            <div class="alert alert-success text-center" role="alert">
                <strong>Well done! ${customer}</strong> You successfully Logged In.
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="product" items="${listProduct}">
                        <tr>
                            <td>${product.productName}</td>
                            <td>${product.productDescription}</td>
                            <td>${product.productPrice}</td>
                            <td><a class="btn btn-success" role="button" href="addToCart/${product.id}">Add to cart</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <br>
        </div>
    </body>
</html>
