<%@ include file="../navigation.jsp" %>

<div class="container">
    <div class="well text-center"><strong>User Sign Up</strong></div>

    <div class="form-group">
        <form:form method="POST" action="signup" commandName="signUpForm">
            First Name :
            <form:input path="firstName"/>
            <form:errors cssClass="error" path="firstName"/>
            Last Name :
            <form:input path="lastName"/>
            <form:errors cssClass="error" path="lastName"/>
            Email Id :
            <form:input path="customerEmail"/>
            <form:errors cssClass="error" path="customerEmail"/>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form:form>
    </div>

</div>

<%@ include file="../footer.jsp" %>