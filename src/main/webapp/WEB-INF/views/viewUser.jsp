<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.*" %>
<%@page import="com.webstore.model.Customer" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="navigation.jsp" %>
<div class="container">

    <div class="well text-center">
        <strong>List of Users</strong>
    </div>

    <table class="table table-bordered table-hover">
        <thead class="thead-inverse">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="customer" items="${listCustomer}">
            <tr>
                <td>${customer.customerEmail}</td>
                <td><a class="btn btn-warning" href="update/${customer.customerEmail}">Edit</a></td>
                <td><a class="btn btn-danger" href="delete/${ccustomer.customerEmail}">Delete</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<%@ include file="footer.jsp" %>